var Students = [];
Students[0] = {
  "name":"Василь",
  "sex":"male",
  "age": 18
  };
Students[1] = {
  "name":"Ганна",
  "sex":"female",
  "age": 19
  };
Students[2] = {
  "name":"Тетяна",
  "sex":"female",
  "age": 19
  };
Students[3] = {
  "name":"Роман",
  "sex":"male",
  "age": 15
  };
Students[4] = {
  "name":"Сара",
  "sex":"female",
  "age": 25
  };

  var result = Students.filter(function(x){return x.age<21 && x.sex=='male'});

  console.log(result);

  var next_result = result.map(function(item, index, result){
  return {item, "yearsUnderAge" : 21-item.age};
});

console.log(next_result);

Students.sort(function(a, b){
if(a.age == b.age) {
  if(a.name < b.name) {
    return -1;
  }
  if(a.name > b.name) {
    return 1;
  }
  return 0;
} else {
    if(a.age < b.age) {
    return -1;
  }
  if(a.age > b.age) {
    return 1;
  }
  return 0;
}

});

console.log(Students);
